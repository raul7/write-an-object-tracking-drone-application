## Write an Object Tracking Drone Application

This is the source code for my article "Write an Object Tracking Drone Application: Using OpenCV, MAVSDK and PX4" published on Circuit Cellar magazine (#362, September 2020).

"Pexels Videos 1572547_540p.mp4" video file belongs to The Lazy Artist Gallery from Pexels (https://www.pexels.com/video/mini-cooper-on-highway-along-the-desert-1572547/)
